<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 15.06.16
 * Time: 17:44
 *
 *
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class LoadImagesData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $albums = $manager->getRepository('AppBundle:Album')->findAll();

        $finder = new Finder();

        $rootDir = $this->container->getParameter('kernel.root_dir');
        $finder->files()->name('/\.(jpg)|(png)|(jpeg)/')->in($rootDir . '/../web/bundles/app/images');

        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        $imageCounter = 0;
        foreach ($finder as $file) {
            $image = new Image();
            $image->setName('Image ' . $imageCounter);
            $image->setPath(str_replace($rootDir . '/../web/', '/', $file->getPathname()));

            // TODO: вот тут поработать еще над логикой связки альбомов и изображений. Надо сделать красиво.
            switch ($imageCounter) {
                case $imageCounter <= 5:
                    $image->setAlbum($albums[0]);
                break;
                default:
                    $image->setAlbum($albums[1]);
            }

            $manager->persist($image);
            $imageCounter++;
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}