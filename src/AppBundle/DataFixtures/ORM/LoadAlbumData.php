<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 14.06.16
 * Time: 17:57
 *
 *
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Album;

class LoadAlbumData implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $albumNames = ['First Album', 'Second Album'];

        foreach ($albumNames as $albumName) {
            $album = new Album();
            $album->setName($albumName);
            $manager->persist($album);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}