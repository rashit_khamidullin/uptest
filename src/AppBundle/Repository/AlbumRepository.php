<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * AlbumRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AlbumRepository extends EntityRepository
{
    public function getAlbums()
    {
        return $this->findAll();
    }

    public function getAlbumsWithNImages($imagesPerAlbum = 10)
    {
        $rsm = new ResultSetMapping($this->getEntityManager());
        $rsm->addEntityResult('AppBundle:Album', 'a');
        $rsm->addFieldResult('a', 'id', 'id');
        $rsm->addFieldResult('a', 'name', 'name');
        $rsm->addJoinedEntityResult('AppBundle:Image' , 'img', 'a', 'images');
        $rsm->addFieldResult('img', 'img_id', 'id');
        $rsm->addFieldResult('img', 'img_name', 'name');
        $rsm->addFieldResult('img', 'path', 'path');

        $q = 'SELECT a.id, a.`name`, img.* FROM album a JOIN (
	SELECT i1.`id` img_id, i1.`album_id`, i1.`NAME` img_name, i1.`path` FROM `image` i1 JOIN `image` i2
		ON i1.`album_id` = i2.`album_id` AND i1.`id` >= i2.`id`
	GROUP BY i1.`album_id`, i1.`NAME`, i1.`path`, i1.id, img_name
	HAVING COUNT(*) <= :images_per_album
) img ON img.`album_id` = a.`id`;';

        $query = $this->getEntityManager()->createNativeQuery($q, $rsm);

        return $query->execute(['images_per_album' => $imagesPerAlbum]);
    }
}
