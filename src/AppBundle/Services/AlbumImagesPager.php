<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 05.07.16
 * Time: 12:29
 *
 *
 */

namespace AppBundle\Services;


use AppBundle\Entity\Album;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Serializer\Serializer;

class AlbumImagesPager
{
    protected $em;
    protected $paginator;
    protected $imagesPerPage;
    protected $serializer;

    public function __construct(EntityManager $em, Paginator $paginator, Serializer $serializer, $imagesPerPage)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->imagesPerPage = $imagesPerPage;
        $this->serializer = $serializer;
    }

    public function paginate(Album $album, $pageNumber)
    {
        /** @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */
        $pagination = $this->paginator->paginate($album->getImages(),$pageNumber,$this->imagesPerPage);
        return $this->serializer->serialize(
            ['pagesCount' => $pagination->getPageCount(), 'images' => $pagination->getItems()],
            'json'
            ,
            array('groups' => array('json_group'))
        );
    }
}