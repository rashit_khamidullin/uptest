<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle::index.html.twig');
    }

    /**
     * @Route("/albums", name="albums")
     * @Method({"GET"})
     */
    public function albumsListAction()
    {
        $repo       = $this->getDoctrine()->getRepository('AppBundle:Album');
        $serializer = $this->get('serializer');

        return new Response(
            $serializer->serialize(
                $repo->getAlbumsWithNImages(2),
                'json',
                array('groups' => array('json_group'))
            ),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/album/{id}/page/{page}", name="album_pages", defaults={"page" = 1}, requirements={
     *     "page": "\d+"
     * })
     * @Method({"GET"})
     */
    public function albumPagesAction(Album $album, $page)
    {
        $paginator = $this->get('app.album.images.pager.service');

        return new Response($paginator->paginate($album, $page), 200, array('Content-Type' => 'application/json'));
    }
}
